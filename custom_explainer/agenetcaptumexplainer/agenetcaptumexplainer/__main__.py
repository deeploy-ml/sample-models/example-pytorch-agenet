import argparse
import logging

from .explainer import CustomExplainer

import kserve
from kserve.errors import ModelMissingError

DEFAULT_EXPLAINER_NAME = "custom_explainer"

parser = argparse.ArgumentParser(parents=[kserve.model_server.parser])
parser.add_argument(
    '--model_name',
    default=DEFAULT_EXPLAINER_NAME,
    help='The name that the model and explainer are served under'
)
args, _ = parser.parse_known_args()

if __name__ == "__main__":

    explainer = CustomExplainer(
        name=DEFAULT_EXPLAINER_NAME
    )
    try:
        explainer.load()
    except Exception as e:
        logging.error(f"Failed to load explainer. {e}")

    kserve.ModelServer().start([explainer])

