import boto3
import datetime
import json

# Export model objects
timestamp_now = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
bucket = 'deeploy-examples'
explainer_custom_image = 'deeployml/agenetcaptumexplainer:0.2.0'
model_object_key = f'pytorch/agenet/{timestamp_now}/model/model-store/model.mar'
model_config_key = f'pytorch/agenet/{timestamp_now}/model/config/config.properties'
s3 = boto3.resource('s3')

s3.meta.client.upload_file('model-store/model.mar', bucket, model_object_key)
s3.meta.client.upload_file('config/config.properties', bucket, model_config_key)

# Add reference to repo
model_reference = {
    'reference': {
        'blob': {
            'url': f's3://{bucket}/pytorch/agenet/{timestamp_now}/model'
            }
        }
    }
explainer_reference = {
    'reference': {
        'docker': {
            'image': explainer_custom_image,
            'uri': '/v1/models/custom_explainer:explain',
            'port': 80
            }
        }
    }

with open('model/reference.json', 'w', encoding='utf-8') as f:
    json.dump(model_reference, f, ensure_ascii=False, indent=4)
with open('explainer/reference.json', 'w', encoding='utf-8') as f:
    json.dump(explainer_reference, f, ensure_ascii=False, indent=4)