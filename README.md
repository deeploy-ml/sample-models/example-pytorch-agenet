# Example PyTorch AgeNet Classification
Example repository that can be used as a reference to deploy a pre-build PyTorch images using Deeploy.

This is the implementation of [Age and Gender Classification by Gil Levi and Tal Hassner](https://talhassner.github.io/home/projects/cnn_agegender/CVPR2015_CNN_AgeGenderEstimation.pdf) using PyTorch based on this origional: https://github.com/droidadroit/age-and-gender-classification
## TorchServe Model archive file
Deeploy expects all model artefects are packaged into a single model archive file (key feature of TorchServe). The `torch-model-archier` CLI can be used to take model checkpoints or model definition file with state_dict and package them into a `.mar` file. For more infromation see [here](https://github.com/pytorch/serve/blob/master/model-archiver/README.md#creating-a-model-archive).

```
torch-model-archiver --model-name model --version 1.0 --model-file pre_trained_model/model.py --serialized-file pre_trained_model/model.pt --handler image_classifier --export-path model-store
```
> Note: if you want to archive using eager mode add the `model-file` flag


Deeploy expects the following model store layout:
```
├── model
│   ├──config
│      ├── config.properties
│   ├── model-store
│      ├── model.mar
```

The config file is designed to work with the Deeploy-KFServing backend.

## Custom Captum Occlusion Explainer
In this repo you find an Custom explainer that can be deployed on Deeploy as a custom Docker (`custom_explainer`). This explainer can clarify the output of Agenet. Occlusion is a perturbation based approach to compute attribution, involving replacing each contiguous rectangular region with a given baseline / reference, and computing the difference in output. More details regarding the occlusion (or grey-box / sliding window) method can be found in the [original paper](https://arxiv.org/abs/1311.2901) and in the [DeepExplain implementation](https://github.com/marcoancona/DeepExplain/blob/master/deepexplain/tensorflow/methods.py#L401).

## Build the Custom Docker Explainer
```
docker build -f Dockerfile -t agenetcustomexplainer
```

## Create/update reference.json files in Deeploy contract
Make sure:
* config/config.properties is present and updated
* model-store/model.mar is present and updated
* Custom explainer docker image is pushed to the image repository of choice
* Variables in create_update_reference.py correspond with blob and image storage of choice.
```
python create_update_reference.py
```

Now you are ready to commit your changes and create a deployment in Deeploy!
